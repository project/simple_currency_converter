### Table of contents

* Introduction
* Usage
* Features
* Basic Installation & Configuration

## Introduction

**Simple Currency Converter** Convert all the prices on your site to any currency for better user experience.

## Usage

**Selecting a Currency Feed**
   - Go to the admin interface and select the desired currency feed.
   - Configure any necessary settings for the feed.

**Converting Prices**
   - Prices on the page will automatically be converted based on the selected currency feed and configuration.

**Handling Conversion Failures**
   - Administrators will receive notifications if a conversion check fails, allowing them to address the issue.
------------
## Features

**Convert Prices on the Page to Any Currency**
   - Prices displayed on the page can be converted to the user's desired currency.

**Multiple Currency Feeds**
   - The system can use different currency feeds for conversion rates:
     - European Central Bank
     - FloatRates
   - Currency feeds are pluggable, allowing developers to write and integrate their own feeds.

**Admin Notification on Conversion Check Failure**
   - Administrators will be notified if a conversion check fails, ensuring they can take prompt action.

**Flexible Configuration**
   - The feature offers flexible configuration options to suit different requirements and preferences.


## Basic Installation & Configuration

**1.Download and enable the module.**

**2. Configure the Module.**

Go to Administration > Configuration > Regional and language > Simple Currency Converter (`admin/config/regional/simplecurrencyconverter`).

**3. Manage Display Settings.**

Click on Manage Display for the relevant content type (`admin/structure/types/manage/page/display`).

**4. Define Your Settings.**

Click on the settings gear to define your settings.
