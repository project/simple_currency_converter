<?php

namespace Drupal\simple_currency_converter;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * @file
 * Defines the default currency list of active currencies from ISO 4217.
 */

/**
 * Currency Converter service.
 */
class CurrencyConverter {
  use StringTranslationTrait;

  const CONVERSION_CACHE_KEY = 'conversion_ratio_list';

  const CONFIG_SIMPLE_CURRENCY_CONVERTER_SETTINGS = 'config:simple_currency_converter.settings';

  /**
   * Provides the config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Provides the module config.
   *
   * @var \Drupal\Core\Config
   */
  protected $config;

  /**
   * Provides the module cache.
   *
   * @var Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Cache Tags Service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  private $cacheTags;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cacheBackend, CacheTagsInvalidator $cacheTags) {
    $this->configFactory = $config_factory;
    $this->config = $config_factory->get('simple_currency_converter.settings');
    $this->cache = $cacheBackend;
    $this->cacheTags = $cacheTags;
  }

  /**
   * List of currency and format rules.
   */
  public function getSupplyCountryInfo() {
    return [
      'AED' => [
        'code' => 'AED',
        'symbol' => 'د.إ',
        'name' => $this->t('United Arab Emirates Dirham'),
        'numeric_code' => '784',
        'code_placement' => 'before',
        'minor_unit' => $this->t('Fils'),
        'major_unit' => $this->t('Dirham'),
      ],
      'AFN' => [
        'code' => 'AFN',
        'symbol' => 'Af',
        'name' => $this->t('Afghan Afghani'),
        'decimals' => 0,
        'numeric_code' => '971',
        'minor_unit' => $this->t('Pul'),
        'major_unit' => $this->t('Afghani'),
      ],
      'ANG' => [
        'code' => 'ANG',
        'symbol' => 'NAf.',
        'name' => $this->t('Netherlands Antillean Guilder'),
        'numeric_code' => '532',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Guilder'),
      ],
      'AOA' => [
        'code' => 'AOA',
        'symbol' => 'Kz',
        'name' => $this->t('Angolan Kwanza'),
        'numeric_code' => '973',
        'minor_unit' => $this->t('Cêntimo'),
        'major_unit' => $this->t('Kwanza'),
      ],
      'ARM' => [
        'code' => 'ARM',
        'symbol' => 'm$n',
        'name' => $this->t('Argentine Peso Moneda Nacional'),
        'minor_unit' => $this->t('Centavos'),
        'major_unit' => $this->t('Peso'),
      ],
      'ARS' => [
        'code' => 'ARS',
        'symbol' => 'AR$',
        'name' => $this->t('Argentine Peso'),
        'numeric_code' => '032',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Peso'),
      ],
      'AUD' => [
        'code' => 'AUD',
        'symbol' => '$',
        'name' => $this->t('Australian Dollar'),
        'numeric_code' => '036',
        'symbol_placement' => 'before',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'AWG' => [
        'code' => 'AWG',
        'symbol' => 'Afl.',
        'name' => $this->t('Aruban Florin'),
        'numeric_code' => '533',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Guilder'),
      ],
      'AZN' => [
        'code' => 'AZN',
        'symbol' => 'man.',
        'name' => $this->t('Azerbaijanian Manat'),
        'minor_unit' => $this->t('Qəpik'),
        'major_unit' => $this->t('New Manat'),
      ],
      'BAM' => [
        'code' => 'BAM',
        'symbol' => 'KM',
        'name' => $this->t('Bosnia-Herzegovina Convertible Mark'),
        'numeric_code' => '977',
        'minor_unit' => $this->t('Fening'),
        'major_unit' => $this->t('Convertible Marka'),
      ],
      'BBD' => [
        'code' => 'BBD',
        'symbol' => 'Bds$',
        'name' => $this->t('Barbadian Dollar'),
        'numeric_code' => '052',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'BDT' => [
        'code' => 'BDT',
        'symbol' => 'Tk',
        'name' => $this->t('Bangladeshi Taka'),
        'numeric_code' => '050',
        'minor_unit' => $this->t('Paisa'),
        'major_unit' => $this->t('Taka'),
      ],
      'BGN' => [
        'code' => 'BGN',
        'symbol' => 'лв',
        'name' => $this->t('Bulgarian lev'),
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'numeric_code' => '975',
        'minor_unit' => $this->t('Stotinka'),
        'major_unit' => $this->t('Lev'),
      ],
      'BHD' => [
        'code' => 'BHD',
        'symbol' => 'BD',
        'name' => $this->t('Bahraini Dinar'),
        'decimals' => 3,
        'numeric_code' => '048',
        'minor_unit' => $this->t('Fils'),
        'major_unit' => $this->t('Dinar'),
      ],
      'BIF' => [
        'code' => 'BIF',
        'symbol' => 'FBu',
        'name' => $this->t('Burundian Franc'),
        'decimals' => 0,
        'numeric_code' => '108',
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Franc'),
      ],
      'BMD' => [
        'code' => 'BMD',
        'symbol' => 'BD$',
        'name' => $this->t('Bermudan Dollar'),
        'numeric_code' => '060',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'BND' => [
        'code' => 'BND',
        'symbol' => 'BN$',
        'name' => $this->t('Brunei Dollar'),
        'numeric_code' => '096',
        'minor_unit' => $this->t('Sen'),
        'major_unit' => $this->t('Dollar'),
      ],
      'BOB' => [
        'code' => 'BOB',
        'symbol' => 'Bs',
        'name' => $this->t('Bolivian Boliviano'),
        'numeric_code' => '068',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Bolivianos'),
      ],
      'BRL' => [
        'code' => 'BRL',
        'symbol' => 'R$',
        'name' => $this->t('Brazilian Real'),
        'numeric_code' => '986',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'thousands_separator' => '.',
        'decimal_separator' => ',',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Reais'),
      ],
      'BSD' => [
        'code' => 'BSD',
        'symbol' => 'BS$',
        'name' => $this->t('Bahamian Dollar'),
        'numeric_code' => '044',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'BTN' => [
        'code' => 'BTN',
        'symbol' => 'Nu.',
        'name' => $this->t('Bhutanese Ngultrum'),
        'numeric_code' => '064',
        'minor_unit' => $this->t('Chetrum'),
        'major_unit' => $this->t('Ngultrum'),
      ],
      'BWP' => [
        'code' => 'BWP',
        'symbol' => 'BWP',
        'name' => $this->t('Botswanan Pula'),
        'numeric_code' => '072',
        'minor_unit' => $this->t('Thebe'),
        'major_unit' => $this->t('Pulas'),
      ],
      'BYR' => [
        'code' => 'BYR',
        'symbol' => 'руб.',
        'name' => $this->t('Belarusian ruble'),
        'numeric_code' => '974',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'decimals' => 0,
        'thousands_separator' => ' ',
        'major_unit' => $this->t('Ruble'),
      ],
      'BZD' => [
        'code' => 'BZD',
        'symbol' => 'BZ$',
        'name' => $this->t('Belize Dollar'),
        'numeric_code' => '084',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'CAD' => [
        'code' => 'CAD',
        'symbol' => 'CA$',
        'name' => $this->t('Canadian Dollar'),
        'numeric_code' => '124',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'CDF' => [
        'code' => 'CDF',
        'symbol' => 'CDF',
        'name' => $this->t('Congolese Franc'),
        'numeric_code' => '976',
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Franc'),
      ],
      'CHF' => [
        'code' => 'CHF',
        'symbol' => 'Fr.',
        'name' => $this->t('Swiss Franc'),
        'rounding_step' => '0.05',
        'numeric_code' => '756',
        'thousands_separator' => "'",
        'minor_unit' => $this->t('Rappen'),
        'major_unit' => $this->t('Franc'),
      ],
      'CLP' => [
        'code' => 'CLP',
        'symbol' => 'CL$',
        'name' => $this->t('Chilean Peso'),
        'decimals' => 0,
        'numeric_code' => '152',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Peso'),
      ],
      'CNY' => [
        'code' => 'CNY',
        'symbol' => '¥',
        'name' => $this->t('Chinese Yuan Renminbi'),
        'numeric_code' => '156',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'thousands_separator' => '',
        'minor_unit' => $this->t('Fen'),
        'major_unit' => $this->t('Yuan'),
      ],
      'COP' => [
        'code' => 'COP',
        'symbol' => '$',
        'name' => $this->t('Colombian Peso'),
        'decimals' => 0,
        'numeric_code' => '170',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'thousands_separator' => '.',
        'decimal_separator' => ',',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Peso'),
      ],
      'CRC' => [
        'code' => 'CRC',
        'symbol' => '¢',
        'name' => $this->t('Costa Rican Colón'),
        'decimals' => 0,
        'numeric_code' => '188',
        'minor_unit' => $this->t('Céntimo'),
        'major_unit' => $this->t('Colón'),
      ],
      'CUC' => [
        'code' => 'CUC',
        'symbol' => 'CUC$',
        'name' => $this->t('Cuban Convertible Peso'),
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Peso'),
      ],
      'CUP' => [
        'code' => 'CUP',
        'symbol' => 'CU$',
        'name' => $this->t('Cuban Peso'),
        'numeric_code' => '192',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Peso'),
      ],
      'CVE' => [
        'code' => 'CVE',
        'symbol' => 'CV$',
        'name' => $this->t('Cape Verdean Escudo'),
        'numeric_code' => '132',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Escudo'),
      ],
      'CZK' => [
        'code' => 'CZK',
        'symbol' => 'Kč',
        'name' => $this->t('Czech Republic Koruna'),
        'numeric_code' => '203',
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Haléř'),
        'major_unit' => $this->t('Koruna'),
      ],
      'DJF' => [
        'code' => 'DJF',
        'symbol' => 'Fdj',
        'name' => $this->t('Djiboutian Franc'),
        'numeric_code' => '262',
        'decimals' => 0,
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Franc'),
      ],
      'DKK' => [
        'code' => 'DKK',
        'symbol' => 'kr.',
        'name' => $this->t('Danish Krone'),
        'numeric_code' => '208',
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Øre'),
        'major_unit' => $this->t('Kroner'),
      ],
      'DOP' => [
        'code' => 'DOP',
        'symbol' => 'RD$',
        'name' => $this->t('Dominican Peso'),
        'numeric_code' => '214',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Peso'),
      ],
      'DZD' => [
        'code' => 'DZD',
        'symbol' => 'DA',
        'name' => $this->t('Algerian Dinar'),
        'numeric_code' => '012',
        'minor_unit' => $this->t('Santeem'),
        'major_unit' => $this->t('Dinar'),
      ],
      'EEK' => [
        'code' => 'EEK',
        'symbol' => 'Ekr',
        'name' => $this->t('Estonian Kroon'),
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'numeric_code' => '233',
        'minor_unit' => $this->t('Sent'),
        'major_unit' => $this->t('Krooni'),
      ],
      'EGP' => [
        'code' => 'EGP',
        'symbol' => 'EG£',
        'name' => $this->t('Egyptian Pound'),
        'numeric_code' => '818',
        'minor_unit' => $this->t('Piastr'),
        'major_unit' => $this->t('Pound'),
      ],
      'ERN' => [
        'code' => 'ERN',
        'symbol' => 'Nfk',
        'name' => $this->t('Eritrean Nakfa'),
        'numeric_code' => '232',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Nakfa'),
      ],
      'ETB' => [
        'code' => 'ETB',
        'symbol' => 'Br',
        'name' => $this->t('Ethiopian Birr'),
        'numeric_code' => '230',
        'minor_unit' => $this->t('Santim'),
        'major_unit' => $this->t('Birr'),
      ],
      'EUR' => [
        'code' => 'EUR',
        'symbol' => '€',
        'name' => $this->t('Euro'),
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'numeric_code' => '978',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Euro'),
      ],
      'FJD' => [
        'code' => 'FJD',
        'symbol' => 'FJ$',
        'name' => $this->t('Fijian Dollar'),
        'numeric_code' => '242',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'FKP' => [
        'code' => 'FKP',
        'symbol' => 'FK£',
        'name' => $this->t('Falkland Islands Pound'),
        'numeric_code' => '238',
        'minor_unit' => $this->t('Penny'),
        'major_unit' => $this->t('Pound'),
      ],
      'GBP' => [
        'code' => 'GBP',
        'symbol' => '£',
        'name' => $this->t('British Pound Sterling'),
        'numeric_code' => '826',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Penny'),
        'major_unit' => $this->t('Pound'),
      ],
      'GHS' => [
        'code' => 'GHS',
        'symbol' => 'GH₵',
        'name' => $this->t('Ghanaian Cedi'),
        'minor_unit' => $this->t('Pesewa'),
        'major_unit' => $this->t('Cedi'),
      ],
      'GIP' => [
        'code' => 'GIP',
        'symbol' => 'GI£',
        'name' => $this->t('Gibraltar Pound'),
        'numeric_code' => '292',
        'minor_unit' => $this->t('Penny'),
        'major_unit' => $this->t('Pound'),
      ],
      'GMD' => [
        'code' => 'GMD',
        'symbol' => 'GMD',
        'name' => $this->t('Gambian Dalasi'),
        'numeric_code' => '270',
        'minor_unit' => $this->t('Butut'),
        'major_unit' => $this->t('Dalasis'),
      ],
      'GNF' => [
        'code' => 'GNF',
        'symbol' => 'FG',
        'name' => $this->t('Guinean Franc'),
        'decimals' => 0,
        'numeric_code' => '324',
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Franc'),
      ],
      'GTQ' => [
        'code' => 'GTQ',
        'symbol' => 'GTQ',
        'name' => $this->t('Guatemalan Quetzal'),
        'numeric_code' => '320',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Quetzales'),
      ],
      'GYD' => [
        'code' => 'GYD',
        'symbol' => 'GY$',
        'name' => $this->t('Guyanaese Dollar'),
        'decimals' => 0,
        'numeric_code' => '328',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'HKD' => [
        'code' => 'HKD',
        'symbol' => 'HK$',
        'name' => $this->t('Hong Kong Dollar'),
        'numeric_code' => '344',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'HNL' => [
        'code' => 'HNL',
        'symbol' => 'HNL',
        'name' => $this->t('Honduran Lempira'),
        'numeric_code' => '340',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Lempiras'),
      ],
      'HRK' => [
        'code' => 'HRK',
        'symbol' => 'kn',
        'name' => $this->t('Croatian Kuna'),
        'numeric_code' => '191',
        'minor_unit' => $this->t('Lipa'),
        'major_unit' => $this->t('Kuna'),
      ],
      'HTG' => [
        'code' => 'HTG',
        'symbol' => 'HTG',
        'name' => $this->t('Haitian Gourde'),
        'numeric_code' => '332',
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Gourde'),
      ],
      'HUF' => [
        'code' => 'HUF',
        'symbol' => 'Ft',
        'name' => $this->t('Hungarian Forint'),
        'numeric_code' => '348',
        'decimal_separator' => ',',
        'thousands_separator' => ' ',
        'decimals' => 0,
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'major_unit' => $this->t('Forint'),
      ],
      'IDR' => [
        'code' => 'IDR',
        'symbol' => 'Rp',
        'name' => $this->t('Indonesian Rupiah'),
        'decimals' => 0,
        'numeric_code' => '360',
        'minor_unit' => $this->t('Sen'),
        'major_unit' => $this->t('Rupiahs'),
      ],
      'ILS' => [
        'code' => 'ILS',
        'symbol' => '₪',
        'name' => $this->t('Israeli New Shekel'),
        'numeric_code' => '376',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Agora'),
        'major_unit' => $this->t('New Shekels'),
      ],
      'INR' => [
        'code' => 'INR',
        'symbol' => 'Rs',
        'name' => $this->t('Indian Rupee'),
        'numeric_code' => '356',
        'minor_unit' => $this->t('Paisa'),
        'major_unit' => $this->t('Rupee'),
      ],
      'IRR' => [
        'code' => 'IRR',
        'symbol' => 'ریال',
        'name' => $this->t('Iranian Rial'),
        'decimals' => 0,
        'numeric_code' => '364',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Rial'),
        'major_unit' => $this->t('Toman'),
      ],
      'ISK' => [
        'code' => 'ISK',
        'symbol' => 'Ikr',
        'name' => $this->t('Icelandic Króna'),
        'decimals' => 0,
        'thousands_separator' => ' ',
        'numeric_code' => '352',
        'minor_unit' => $this->t('Eyrir'),
        'major_unit' => $this->t('Kronur'),
      ],
      'JMD' => [
        'code' => 'JMD',
        'symbol' => 'J$',
        'name' => $this->t('Jamaican Dollar'),
        'numeric_code' => '388',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'JOD' => [
        'code' => 'JOD',
        'symbol' => 'JD',
        'name' => $this->t('Jordanian Dinar'),
        'decimals' => 3,
        'numeric_code' => '400',
        'minor_unit' => $this->t('Piastr'),
        'major_unit' => $this->t('Dinar'),
      ],
      'JPY' => [
        'code' => 'JPY',
        'symbol' => '¥',
        'name' => $this->t('Japanese Yen'),
        'decimals' => 0,
        'numeric_code' => '392',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Sen'),
        'major_unit' => $this->t('Yen'),
      ],
      'KES' => [
        'code' => 'KES',
        'symbol' => 'Ksh',
        'name' => $this->t('Kenyan Shilling'),
        'numeric_code' => '404',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Shilling'),
      ],
      'KGS' => [
        'code' => 'KGS',
        'code_placement' => 'hidden',
        'symbol' => 'сом',
        'symbol_placement' => 'after',
        'name' => $this->t('Kyrgyzstani Som'),
        'numeric_code' => '417',
        'thousands_separator' => '',
        'major_unit' => $this->t('Som'),
        'minor_unit' => $this->t('Tyiyn'),
      ],
      'KMF' => [
        'code' => 'KMF',
        'symbol' => 'CF',
        'name' => $this->t('Comorian Franc'),
        'decimals' => 0,
        'numeric_code' => '174',
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Franc'),
      ],
      'KRW' => [
        'code' => 'KRW',
        'symbol' => '₩',
        'name' => $this->t('South Korean Won'),
        'decimals' => 0,
        'numeric_code' => '410',
        'minor_unit' => $this->t('Jeon'),
        'major_unit' => $this->t('Won'),
      ],
      'KWD' => [
        'code' => 'KWD',
        'symbol' => 'KD',
        'name' => $this->t('Kuwaiti Dinar'),
        'decimals' => 3,
        'numeric_code' => '414',
        'minor_unit' => $this->t('Fils'),
        'major_unit' => $this->t('Dinar'),
      ],
      'KYD' => [
        'code' => 'KYD',
        'symbol' => 'KY$',
        'name' => $this->t('Cayman Islands Dollar'),
        'numeric_code' => '136',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'KZT' => [
        'code' => 'KZT',
        'symbol' => 'тг.',
        'name' => $this->t('Kazakhstani tenge'),
        'numeric_code' => '398',
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Tiyn'),
        'major_unit' => $this->t('Tenge'),
      ],
      'LAK' => [
        'code' => 'LAK',
        'symbol' => '₭N',
        'name' => $this->t('Laotian Kip'),
        'decimals' => 0,
        'numeric_code' => '418',
        'minor_unit' => $this->t('Att'),
        'major_unit' => $this->t('Kips'),
      ],
      'LBP' => [
        'code' => 'LBP',
        'symbol' => 'LB£',
        'name' => $this->t('Lebanese Pound'),
        'decimals' => 0,
        'numeric_code' => '422',
        'minor_unit' => $this->t('Piastre'),
        'major_unit' => $this->t('Pound'),
      ],
      'LKR' => [
        'code' => 'LKR',
        'symbol' => 'SLRs',
        'name' => $this->t('Sri Lanka Rupee'),
        'numeric_code' => '144',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Rupee'),
      ],
      'LRD' => [
        'code' => 'LRD',
        'symbol' => 'L$',
        'name' => $this->t('Liberian Dollar'),
        'numeric_code' => '430',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'LSL' => [
        'code' => 'LSL',
        'symbol' => 'LSL',
        'name' => $this->t('Lesotho Loti'),
        'numeric_code' => '426',
        'minor_unit' => $this->t('Sente'),
        'major_unit' => $this->t('Loti'),
      ],
      'LTL' => [
        'code' => 'LTL',
        'symbol' => 'Lt',
        'name' => $this->t('Lithuanian Litas'),
        'numeric_code' => '440',
        'minor_unit' => $this->t('Centas'),
        'major_unit' => $this->t('Litai'),
      ],
      'LVL' => [
        'code' => 'LVL',
        'symbol' => 'Ls',
        'name' => $this->t('Latvian Lats'),
        'numeric_code' => '428',
        'minor_unit' => $this->t('Santims'),
        'major_unit' => $this->t('Lati'),
      ],
      'LYD' => [
        'code' => 'LYD',
        'symbol' => 'LD',
        'name' => $this->t('Libyan Dinar'),
        'decimals' => 3,
        'numeric_code' => '434',
        'minor_unit' => $this->t('Dirham'),
        'major_unit' => $this->t('Dinar'),
      ],
      'MAD' => [
        'code' => 'MAD',
        'symbol' => ' Dhs',
        'name' => $this->t('Moroccan Dirham'),
        'numeric_code' => '504',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Santimat'),
        'major_unit' => $this->t('Dirhams'),
      ],
      'MDL' => [
        'code' => 'MDL',
        'symbol' => 'MDL',
        'name' => $this->t('Moldovan leu'),
        'symbol_placement' => 'after',
        'numeric_code' => '498',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('bani'),
        'major_unit' => $this->t('Lei'),
      ],
      'MKD' => [
        'code' => 'MKD',
        'symbol' => 'ден',
        'name' => $this->t('Macedonian denar'),
        'symbol_placement' => 'after',
        'numeric_code' => '807',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Deni'),
        'major_unit' => $this->t('Denari'),
      ],
      'MMK' => [
        'code' => 'MMK',
        'symbol' => 'MMK',
        'name' => $this->t('Myanma Kyat'),
        'decimals' => 0,
        'numeric_code' => '104',
        'minor_unit' => $this->t('Pya'),
        'major_unit' => $this->t('Kyat'),
      ],
      'MNT' => [
        'code' => 'MNT',
        'symbol' => '₮',
        'name' => $this->t('Mongolian Tugrik'),
        'decimals' => 0,
        'numeric_code' => '496',
        'minor_unit' => $this->t('Möngö'),
        'major_unit' => $this->t('Tugriks'),
      ],
      'MOP' => [
        'code' => 'MOP',
        'symbol' => 'MOP$',
        'name' => $this->t('Macanese Pataca'),
        'numeric_code' => '446',
        'minor_unit' => $this->t('Avo'),
        'major_unit' => $this->t('Pataca'),
      ],
      'MRO' => [
        'code' => 'MRO',
        'symbol' => 'UM',
        'name' => $this->t('Mauritanian Ouguiya'),
        'decimals' => 0,
        'numeric_code' => '478',
        'minor_unit' => $this->t('Khoums'),
        'major_unit' => $this->t('Ouguiya'),
      ],
      'MTP' => [
        'code' => 'MTP',
        'symbol' => 'MT£',
        'name' => $this->t('Maltese Pound'),
        'minor_unit' => $this->t('Shilling'),
        'major_unit' => $this->t('Pound'),
      ],
      'MUR' => [
        'code' => 'MUR',
        'symbol' => 'MURs',
        'name' => $this->t('Mauritian Rupee'),
        'decimals' => 0,
        'numeric_code' => '480',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Rupee'),
      ],
      'MXN' => [
        'code' => 'MXN',
        'symbol' => '$',
        'name' => $this->t('Mexican Peso'),
        'numeric_code' => '484',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Peso'),
      ],
      'MYR' => [
        'code' => 'MYR',
        'symbol' => 'RM',
        'name' => $this->t('Malaysian Ringgit'),
        'numeric_code' => '458',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Sen'),
        'major_unit' => $this->t('Ringgits'),
      ],
      'MZN' => [
        'code' => 'MZN',
        'symbol' => 'MTn',
        'name' => $this->t('Mozambican Metical'),
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Metical'),
      ],
      'NAD' => [
        'code' => 'NAD',
        'symbol' => 'N$',
        'name' => $this->t('Namibian Dollar'),
        'numeric_code' => '516',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'NGN' => [
        'code' => 'NGN',
        'symbol' => '₦',
        'name' => $this->t('Nigerian Naira'),
        'numeric_code' => '566',
        'minor_unit' => $this->t('Kobo'),
        'major_unit' => $this->t('Naira'),
      ],
      'NIO' => [
        'code' => 'NIO',
        'symbol' => 'C$',
        'name' => $this->t('Nicaraguan Cordoba Oro'),
        'numeric_code' => '558',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Cordoba'),
      ],
      'NOK' => [
        'code' => 'NOK',
        'symbol' => 'Nkr',
        'name' => $this->t('Norwegian Krone'),
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'numeric_code' => '578',
        'minor_unit' => $this->t('Øre'),
        'major_unit' => $this->t('Krone'),
      ],
      'NPR' => [
        'code' => 'NPR',
        'symbol' => 'NPRs',
        'name' => $this->t('Nepalese Rupee'),
        'numeric_code' => '524',
        'minor_unit' => $this->t('Paisa'),
        'major_unit' => $this->t('Rupee'),
      ],
      'NZD' => [
        'code' => 'NZD',
        'symbol' => '$',
        'name' => $this->t('New Zealand Dollar'),
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'numeric_code' => '554',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'PAB' => [
        'code' => 'PAB',
        'symbol' => 'B/.',
        'name' => $this->t('Panamanian Balboa'),
        'numeric_code' => '590',
        'minor_unit' => $this->t('Centésimo'),
        'major_unit' => $this->t('Balboa'),
      ],
      'PEN' => [
        'code' => 'PEN',
        'symbol' => 'S/.',
        'name' => $this->t('Peruvian Nuevo Sol'),
        'numeric_code' => '604',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Céntimo'),
        'major_unit' => $this->t('Nuevos Sole'),
      ],
      'PGK' => [
        'code' => 'PGK',
        'symbol' => 'PGK',
        'name' => $this->t('Papua New Guinean Kina'),
        'numeric_code' => '598',
        'minor_unit' => $this->t('Toea'),
        'major_unit' => $this->t('Kina'),
      ],
      'PHP' => [
        'code' => 'PHP',
        'symbol' => '₱',
        'name' => $this->t('Philippine Peso'),
        'numeric_code' => '608',
        'minor_unit' => $this->t('Centavo'),
        'major_unit' => $this->t('Peso'),
      ],
      'PKR' => [
        'code' => 'PKR',
        'symbol' => 'PKRs',
        'name' => $this->t('Pakistani Rupee'),
        'decimals' => 0,
        'numeric_code' => '586',
        'minor_unit' => $this->t('Paisa'),
        'major_unit' => $this->t('Rupee'),
      ],
      'PLN' => [
        'code' => 'PLN',
        'symbol' => 'zł',
        'name' => $this->t('Polish Złoty'),
        'decimal_separator' => ',',
        'thousands_separator' => ' ',
        'numeric_code' => '985',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Grosz'),
        'major_unit' => $this->t('Złotych'),
      ],
      'PYG' => [
        'code' => 'PYG',
        'symbol' => '₲',
        'name' => $this->t('Paraguayan Guarani'),
        'decimals' => 0,
        'numeric_code' => '600',
        'minor_unit' => $this->t('Céntimo'),
        'major_unit' => $this->t('Guarani'),
      ],
      'QAR' => [
        'code' => 'QAR',
        'symbol' => 'QR',
        'name' => $this->t('Qatari Rial'),
        'numeric_code' => '634',
        'minor_unit' => $this->t('Dirham'),
        'major_unit' => $this->t('Rial'),
      ],
      'RHD' => [
        'code' => 'RHD',
        'symbol' => 'RH$',
        'name' => $this->t('Rhodesian Dollar'),
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'RON' => [
        'code' => 'RON',
        'symbol' => 'RON',
        'name' => $this->t('Romanian Leu'),
        'minor_unit' => $this->t('Ban'),
        'major_unit' => $this->t('Leu'),
      ],
      'RSD' => [
        'code' => 'RSD',
        'symbol' => 'din.',
        'name' => $this->t('Serbian Dinar'),
        'decimals' => 0,
        'minor_unit' => $this->t('Para'),
        'major_unit' => $this->t('Dinars'),
      ],
      'RUB' => [
        'code' => 'RUB',
        'symbol' => 'руб.',
        'name' => $this->t('Russian Ruble'),
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'numeric_code' => '643',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Kopek'),
        'major_unit' => $this->t('Ruble'),
      ],
      'SAR' => [
        'code' => 'SAR',
        'symbol' => 'SR',
        'name' => $this->t('Saudi Riyal'),
        'numeric_code' => '682',
        'minor_unit' => $this->t('Hallallah'),
        'major_unit' => $this->t('Riyals'),
      ],
      'SBD' => [
        'code' => 'SBD',
        'symbol' => 'SI$',
        'name' => $this->t('Solomon Islands Dollar'),
        'numeric_code' => '090',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'SCR' => [
        'code' => 'SCR',
        'symbol' => 'SRe',
        'name' => $this->t('Seychellois Rupee'),
        'numeric_code' => '690',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Rupee'),
      ],
      'SDD' => [
        'code' => 'SDD',
        'symbol' => 'LSd',
        'name' => $this->t('Old Sudanese Dinar'),
        'numeric_code' => '736',
        'minor_unit' => $this->t('None'),
        'major_unit' => $this->t('Dinar'),
      ],
      'SEK' => [
        'code' => 'SEK',
        'symbol' => 'kr',
        'name' => $this->t('Swedish Krona'),
        'numeric_code' => '752',
        'thousands_separator' => ' ',
        'decimal_separator' => ',',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Öre'),
        'major_unit' => $this->t('Kronor'),
      ],
      'SGD' => [
        'code' => 'SGD',
        'symbol' => 'S$',
        'name' => $this->t('Singapore Dollar'),
        'numeric_code' => '702',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'SHP' => [
        'code' => 'SHP',
        'symbol' => 'SH£',
        'name' => $this->t('Saint Helena Pound'),
        'numeric_code' => '654',
        'minor_unit' => $this->t('Penny'),
        'major_unit' => $this->t('Pound'),
      ],
      'SLL' => [
        'code' => 'SLL',
        'symbol' => 'Le',
        'name' => $this->t('Sierra Leonean Leone'),
        'decimals' => 0,
        'numeric_code' => '694',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Leone'),
      ],
      'SOS' => [
        'code' => 'SOS',
        'symbol' => 'Ssh',
        'name' => $this->t('Somali Shilling'),
        'decimals' => 0,
        'numeric_code' => '706',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Shilling'),
      ],
      'SRD' => [
        'code' => 'SRD',
        'symbol' => 'SR$',
        'name' => $this->t('Surinamese Dollar'),
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'SRG' => [
        'code' => 'SRG',
        'symbol' => 'Sf',
        'name' => $this->t('Suriname Guilder'),
        'numeric_code' => '740',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Guilder'),
      ],
      'STD' => [
        'code' => 'STD',
        'symbol' => 'Db',
        'name' => $this->t('São Tomé and Príncipe Dobra'),
        'decimals' => 0,
        'numeric_code' => '678',
        'minor_unit' => $this->t('Cêntimo'),
        'major_unit' => $this->t('Dobra'),
      ],
      'SYP' => [
        'code' => 'SYP',
        'symbol' => 'SY£',
        'name' => $this->t('Syrian Pound'),
        'decimals' => 0,
        'numeric_code' => '760',
        'minor_unit' => $this->t('Piastre'),
        'major_unit' => $this->t('Pound'),
      ],
      'SZL' => [
        'code' => 'SZL',
        'symbol' => 'SZL',
        'name' => $this->t('Swazi Lilangeni'),
        'numeric_code' => '748',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Lilangeni'),
      ],
      'THB' => [
        'code' => 'THB',
        'symbol' => '฿',
        'name' => $this->t('Thai Baht'),
        'numeric_code' => '764',
        'minor_unit' => $this->t('Satang'),
        'major_unit' => $this->t('Baht'),
      ],
      'TND' => [
        'code' => 'TND',
        'symbol' => 'DT',
        'name' => $this->t('Tunisian Dinar'),
        'decimals' => 3,
        'numeric_code' => '788',
        'minor_unit' => $this->t('Millime'),
        'major_unit' => $this->t('Dinar'),
      ],
      'TOP' => [
        'code' => 'TOP',
        'symbol' => 'T$',
        'name' => $this->t('Tongan Paʻanga'),
        'numeric_code' => '776',
        'minor_unit' => $this->t('Senit'),
        'major_unit' => $this->t('Paʻanga'),
      ],
      'TRY' => [
        'code' => 'TRY',
        'symbol' => 'TL',
        'name' => $this->t('Turkish Lira'),
        'numeric_code' => '949',
        'thousands_separator' => '.',
        'decimal_separator' => ',',
        'symbol_placement' => 'after',
        'code_placement' => '',
        'minor_unit' => $this->t('Kurus'),
        'major_unit' => $this->t('Lira'),
      ],
      'TTD' => [
        'code' => 'TTD',
        'symbol' => 'TT$',
        'name' => $this->t('Trinidad and Tobago Dollar'),
        'numeric_code' => '780',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'TWD' => [
        'code' => 'TWD',
        'symbol' => 'NT$',
        'name' => $this->t('New Taiwan Dollar'),
        'numeric_code' => '901',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('New Dollar'),
      ],
      'TZS' => [
        'code' => 'TZS',
        'symbol' => 'TSh',
        'name' => $this->t('Tanzanian Shilling'),
        'decimals' => 0,
        'numeric_code' => '834',
        'minor_unit' => $this->t('Senti'),
        'major_unit' => $this->t('Shilling'),
      ],
      'UAH' => [
        'code' => 'UAH',
        'symbol' => 'грн.',
        'name' => $this->t('Ukrainian Hryvnia'),
        'numeric_code' => '980',
        'thousands_separator' => '',
        'decimal_separator' => '.',
        'symbol_placement' => 'after',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Kopiyka'),
        'major_unit' => $this->t('Hryvnia'),
      ],
      'UGX' => [
        'code' => 'UGX',
        'symbol' => 'USh',
        'name' => $this->t('Ugandan Shilling'),
        'decimals' => 0,
        'numeric_code' => '800',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Shilling'),
      ],
      'USD' => [
        'code' => 'USD',
        'symbol' => '$',
        'name' => $this->t('United States Dollar'),
        'numeric_code' => '840',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'UYU' => [
        'code' => 'UYU',
        'symbol' => '$U',
        'name' => $this->t('Uruguayan Peso'),
        'numeric_code' => '858',
        'minor_unit' => $this->t('Centésimo'),
        'major_unit' => $this->t('Peso'),
      ],
      'VEF' => [
        'code' => 'VEF',
        'symbol' => 'Bs.F.',
        'name' => $this->t('Venezuelan Bolívar Fuerte'),
        'minor_unit' => $this->t('Céntimo'),
        'major_unit' => $this->t('Bolivares Fuerte'),
      ],
      'VND' => [
        'code' => 'VND',
        'symbol' => 'đ',
        'name' => $this->t('Vietnamese Dong'),
        'decimals' => 0,
        'thousands_separator' => '.',
        'symbol_placement' => 'after',
        'symbol_spacer' => '',
        'code_placement' => 'hidden',
        'numeric_code' => '704',
        'minor_unit' => $this->t('Hà'),
        'major_unit' => $this->t('Dong'),
      ],
      'VUV' => [
        'code' => 'VUV',
        'symbol' => 'VT',
        'name' => $this->t('Vanuatu Vatu'),
        'decimals' => 0,
        'numeric_code' => '548',
        'major_unit' => $this->t('Vatu'),
      ],
      'WST' => [
        'code' => 'WST',
        'symbol' => 'WS$',
        'name' => $this->t('Samoan Tala'),
        'numeric_code' => '882',
        'minor_unit' => $this->t('Sene'),
        'major_unit' => $this->t('Tala'),
      ],
      'XAF' => [
        'code' => 'XAF',
        'symbol' => 'FCFA',
        'name' => $this->t('CFA Franc BEAC'),
        'decimals' => 0,
        'numeric_code' => '950',
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Franc'),
      ],
      'XCD' => [
        'code' => 'XCD',
        'symbol' => 'EC$',
        'name' => $this->t('East Caribbean Dollar'),
        'numeric_code' => '951',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Dollar'),
      ],
      'XOF' => [
        'code' => 'XOF',
        'symbol' => 'CFA',
        'name' => $this->t('CFA Franc BCEAO'),
        'decimals' => 0,
        'numeric_code' => '952',
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Franc'),
      ],
      'XPF' => [
        'code' => 'XPF',
        'symbol' => 'CFPF',
        'name' => $this->t('CFP Franc'),
        'decimals' => 0,
        'numeric_code' => '953',
        'minor_unit' => $this->t('Centime'),
        'major_unit' => $this->t('Franc'),
      ],
      'YER' => [
        'code' => 'YER',
        'symbol' => 'YR',
        'name' => $this->t('Yemeni Rial'),
        'decimals' => 0,
        'numeric_code' => '886',
        'minor_unit' => $this->t('Fils'),
        'major_unit' => $this->t('Rial'),
      ],
      'ZAR' => [
        'code' => 'ZAR',
        'symbol' => 'R',
        'name' => $this->t('South African Rand'),
        'numeric_code' => '710',
        'symbol_placement' => 'before',
        'code_placement' => 'hidden',
        'minor_unit' => $this->t('Cent'),
        'major_unit' => $this->t('Rand'),
      ],
      'ZMK' => [
        'code' => 'ZMK',
        'symbol' => 'ZK',
        'name' => $this->t('Zambian Kwacha'),
        'decimals' => 0,
        'numeric_code' => '894',
        'minor_unit' => $this->t('Ngwee'),
        'major_unit' => $this->t('Kwacha'),
      ],
    ];
  }

  /**
   * Retrieve an array of currencies.
   */
  public function getSupplyCountryCodesOptions() {
    $codes = $this->getSupplyCountryInfo();
    $get_name = function ($country_element) {
      return $country_element['name'];
    };

    return array_map($get_name, $codes);
  }

  /**
   * Get the currency rate.
   *
   * @param string $from_currency
   *   The currency to convert from.
   * @param string $to_currency
   *   The currency to convert to.
   * @param int $amount
   *   The amount to convert.
   *
   * @return float|null
   *   The converted amount
   */
  public function convert($from_currency, $to_currency, $amount) {
    $feed = $this->requestFeed('primary', $from_currency, $to_currency, $amount);

    if (!$feed) {
      $feed = $this->requestFeed('secondary', $from_currency, $to_currency, $amount);

      $result = \Drupal::hasService('currency_converter_notifier');

      if ($result) {
        \Drupal::service('currency_converter_notifier')->notify(compact('from_currency', 'to_currency', 'feed'));
      }
    }

    if ($feed) {
      return round($feed, 5);
    }

    return NULL;
  }

  /**
   * Get the currency feed.
   */
  private function requestFeed($type, $from_currency, $to_currency, $amount) {
    $output = NULL;

    $service = NULL;

    switch ($type) {
      case 'primary':
        $service = $this->config->get('feed_primary');

        break;

      case 'secondary':
        $service = $this->config->get('feed_secondary');

        break;
    }

    if ($service) {
      /**
       * @var \Drupal\simple_currency_converter\CurrencyConverter\CurrencyConverterInterface
       */
      $service = \Drupal::service($service);

      $output = $service->convert($from_currency, $to_currency, $amount);
    }

    return $output;
  }

  /**
   * Get unique name for conversion.
   */
  public function getConversionKeyValue($from_currency, $to_currency): string {
    return 'scc_ratio_from_' . $from_currency . '_to_' . $to_currency;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpireTime() {
    return \Drupal::time()
      ->getRequestTime() +
      $this->getExpireTimeFromConfig();
  }

  /**
   * {@inheritdoc}
   */
  public function getExpireTimeFromConfig() {
    return $this->getSetting('conversion_rate_lifetime');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultCurrency() {
    return $this->getSetting('default_conversion_currency');
  }

  /**
   * {@inheritdoc}
   */
  protected function getSetting($setting) {
    return $this->config->get($setting);
  }

  /**
   * {@inheritdoc}
   */
  public function getConversionCacheList() {
    return $this->cache->get(self::CONVERSION_CACHE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public function setCache(array $data = []) {
    $this->cacheTags->invalidateTags([self::CONFIG_SIMPLE_CURRENCY_CONVERTER_SETTINGS]);
    $this->cache->set(self::CONVERSION_CACHE_KEY, $data, $this->getExpireTime());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigKey() {
    return self::CONFIG_SIMPLE_CURRENCY_CONVERTER_SETTINGS;
  }

}
