<?php

namespace Drupal\simple_currency_converter\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\simple_currency_converter\CurrencyConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for config settings.
 */
class AdminForm extends ConfigFormBase {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public $configFactory;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\Config
   */
  public $config;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  public $state;

  /**
   * Currency Converter.
   *
   * @var object
   */
  public $currencyConverters;

  /**
   * Default Currency Converter.
   *
   * @var object
   */
  public $defaultCurrencyConverter;

  /**
   * Currency Converter Service.
   *
   * @var \Drupal\simple_currency_converter\CurrencyConverter
   */
  public $currencyConverterService;

  /**
   * Constructs a new form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state keyvalue collection to use.
   * @param object $currencyConverters
   *   The Currency Converter.
   * @param object $defaultCurrencyConverter
   *   The Default Currency Converter.
   * @param \Drupal\simple_currency_converter\CurrencyConverter $currencyConverterService
   *   The Currency Converter Service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    $currencyConverters,
    $defaultCurrencyConverter,
    CurrencyConverter $currencyConverterService,
  ) {
    parent::__construct($config_factory);

    $this->configFactory = $config_factory;
    $this->config = $config_factory->getEditable('simple_currency_converter.settings');
    $this->state = $state;
    $this->currencyConverters = $currencyConverters;
    $this->defaultCurrencyConverter = $defaultCurrencyConverter;
    $this->currencyConverterService = $currencyConverterService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->getParameter('simple_currency_converters'),
      $container->getParameter('simple_currency_converter_default'),
      $container->get('simple_currency_converter.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_currency_converter_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['simple_currency_converter.settings'];
  }

  /**
   * Build the form.
   *
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $currency_converters = $this->currencyConverters;
    $total = count($currency_converters);

    $form['currency_converters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Available Currency Converters'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $fieldset = &$form['currency_converters'];

    if (!$total) {
      $fieldset['#description'] = $this->t('Please enable at least one currency converter provider module.');
    }
    else {
      $options = [
        '' => $this->t('None'),
      ];

      foreach ($currency_converters as $key => $value) {
        $options[$key] = $value['title'];
      }

      $key = 'feed_primary';

      $default_value = $this->config->get($key);

      $fieldset[$key] = [
        '#type' => 'select',
        '#title' => $this->t('Choose the primary currency converter to use'),
        '#options' => $options,
        '#default_value' => $default_value,
      ];

      if ($total > 1) {
        $key = 'feed_secondary';

        $default_value = $this->config->get($key);

        $fieldset[$key] = [
          '#type' => 'select',
          '#title' => $this->t('Choose the secondary currency converter to use'),
          '#options' => $options,
          '#default_value' => $default_value,
        ];
      }

      $codes = $this->currencyConverterService->getSupplyCountryInfo();

      foreach ($currency_converters as $key => $value) {
        $service = \Drupal::service($key);

        $fieldset[$key] = [
          '#type' => 'fieldset',
          '#title' => $value['title'],
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        ];

        $currencies = $service->currencies();

        $markup = '';

        foreach ($currencies as $currency) {
          $markup .= '<li>' . $currency . ' => ' . $codes[$currency]['name'] . '</li>';
        }

        $markup = $this->t('The following currencies are provided:') . '<ul>' . $markup . '</ul>';

        $fieldset[$key]['markup']['#markup'] = $markup;
      }
    }

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration variables'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $fieldset = &$form['settings'];

    $options = $this->currencyConverterService->getSupplyCountryCodesOptions();
    $options = array_merge(['none' => $this->t('-- None --')], $options);

    $key = 'default_conversion_currency';

    $default_value = $this->config->get($key);

    $description = $this->t('Please choose a default currency to use for the conversion.  Note: This is the currency that is initially used on the page.');

    $fieldset[$key] = [
      '#type' => 'select',
      '#title' => $this->t('Default conversion currency'),
      '#options' => $options,
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    $key = 'conversion_rate_lifetime';

    $default_value = $this->config->get($key);

    $description = $this->t('How many seconds should the currency conversion value last for, before refreshing?');

    $fieldset[$key] = [
      '#type' => 'textfield',
      '#title' => $this->t('Conversion Rate Lifetime'),
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    $key = 'default_storage';
    $default_value = $this->config->get($key);
    $options = ['cache' => $this->t('Cache'), 'cookie' => $this->t('Cookie')];
    $description = $this->t('How the conversion rate is stored (cache method recommended).');
    $fieldset[$key] = [
      '#type' => 'radios',
      '#title' => $this->t('Default storage'),
      '#options' => $options,
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    $key = 'window_trigger';

    $default_value = $this->config->get($key);

    $description = $this->t('Please specify the CSS selector to use to make the converter modal window scroll down and up.');

    $fieldset[$key] = [
      '#type' => 'textfield',
      '#title' => $this->t('Modal Window Activation Selector'),
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    $key = 'window_id';

    $default_value = $this->config->get($key);

    $description = $this->t('Please specify a CSS ID to use for placement of the converter dropdown, where users can choose a currency');

    $fieldset[$key] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form holder element ID'),
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    $key = 'window_title';

    $default_value = $this->config->get($key);

    $description = $this->t('Clicking on element with this ID will close the form');

    $fieldset[$key] = [
      '#type' => 'textfield',
      '#title' => $this->t('Modal Window Title'),
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    $key = 'conversion_selector';

    $default_value = $this->config->get($key);

    $description = $this->t('Please specify the CSS selector for the currency to convert.');

    $fieldset[$key] = [
      '#type' => 'textfield',
      '#title' => $this->t('Price elements to convert'),
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    $key = 'disclaimer';

    $default_value = $this->config->get($key);

    $description = $this->t('Please specify the disclaimer text to display in the modal window.');

    $fieldset[$key] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disclaimer'),
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $keys = [
      'feed_primary',
      'feed_secondary',
      'window_id',
      'window_trigger',
      'window_title',
      'conversion_selector',
      'disclaimer',
      'default_conversion_currency',
      'conversion_rate_lifetime',
      'default_storage',
    ];

    foreach ($keys as $key) {
      $this->config->set($key, $form_state->getValue($key));
    }

    $this->config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

}
