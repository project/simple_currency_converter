<?php

namespace Drupal\simple_currency_converter\CurrencyConverter;

/**
 * Currency Converter Interface.
 */
interface CurrencyConverterInterface {

  /**
   * Converts an amount from one currency to another.
   */
  public function convert($from_currency, $to_currency, $amount);

  /**
   * Returns an array of supported currencies.
   */
  public function currencies();

}
