<?php

namespace Drupal\simple_currency_converter\Commands;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\simple_currency_converter\CurrencyConverter;
use Drush\Commands\DrushCommands;

/**
 * Provides drush commands for Fastly.
 */
class ProviderCommands extends DrushCommands {

  /**
   * The Currency Converter.
   *
   * @var \Drupal\simple_currency_converter\CurrencyConverter
   */
  protected $currencyConverter;

  /**
   * Provides the module cache.
   *
   * @var \Drupal\Core\Cache\Cache
   */
  protected $cache;

  /**
   * ProviderCommands constructor.
   *
   * @param \Drupal\simple_currency_converter\CurrencyConverter $currencyConverter
   *   Currency Converter.
   * @param Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend.
   */
  public function __construct(
    CurrencyConverter $currencyConverter,
    CacheBackendInterface $cacheBackend,
  ) {
    parent::__construct();
    $this->currencyConverter = $currencyConverter;
    $this->cache = $cacheBackend;
  }

  /**
   * SCC Providers/Update conversion currencies to cache.
   *
   * @param string $currencies
   *   Comma seperated ist of currencies to update conversion rations in cache.
   *
   * @command scc:update-conversion-rates
   * @aliases scc-ucr
   *
   * @usage scc:update-conversion-rates USD,AUD,NZD,GBP,SGD
   */
  public function updateConversionRates(string $currencies) {
    $cache_data = [];
    $current_cache_list = $this->cache->get('conversion_ratio_list');
    if ($current_cache_list !== FALSE && property_exists($current_cache_list, 'data')) {
      $cache_data = $current_cache_list->data;
    }
    $default_currency = $this->currencyConverter->getDefaultCurrency();
    $currency_codes_list = $this->currencyConverter->getSupplyCountryCodesOptions();

    $currency_list = explode(',', $currencies);
    foreach ($currency_list as $currency) {
      if (array_key_exists(strtoupper($currency), $currency_codes_list)) {
        $conversion_ratio_key = $this->currencyConverter->getConversionKeyValue($default_currency, strtoupper($currency));
        $ratio = $this->currencyConverter->convert($default_currency, strtoupper($currency), 1);
        $cache_data[$conversion_ratio_key] = $ratio;
      }
    }

    if (!empty($cache_data)) {
      $this->currencyConverter->setCache($cache_data);
    }
  }

}
