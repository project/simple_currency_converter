<?php

namespace Drupal\simple_currency_converter;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\simple_currency_converter\Compiler\CurrencyConvertersPass;

/**
 * Simple currency converter.
 */
class SimpleCurrencyConverterServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new CurrencyConvertersPass());
  }

}
