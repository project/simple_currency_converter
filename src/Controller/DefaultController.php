<?php

namespace Drupal\simple_currency_converter\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\simple_currency_converter\CurrencyConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Default controller for the simple_currency_converter module.
 */
class DefaultController extends ControllerBase {

  /**
   * Provides the config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Provides the module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Provides the module cache.
   *
   * @var \Drupal\Core\Cache\Cache
   */
  protected $cache;

  /**
   * Provides the module c.
   *
   * @var \Drupal\simple_currency_converter\CurrencyConverter
   */
  protected $currencyConverter;

  /**
   * Constructs a new object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cacheBackend, CurrencyConverter $currencyConverter) {
    $this->configFactory = $config_factory;
    $this->cache = $cacheBackend;
    $this->currencyConverter = $currencyConverter;

    $this->config = $config_factory->get('simple_currency_converter.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.default'),
      $container->get('simple_currency_converter.default')
    );
  }

  /**
   * Set currency conversion ratio.
   */
  public function setCurrency($from_currency, $to_currency) {
    if ($this->config->get('default_storage') === 'cache') {
      return $this->setCurrencyToCache($from_currency, $to_currency);
    }
    return $this->setCurrencyToCookie($from_currency, $to_currency);
  }

  /**
   * Set currency in cookie.
   */
  public function setCurrencyToCookie($from_currency, $to_currency) {
    $conversion_ratio = 1;

    if ($from_currency != $to_currency) {
      $conversion_ratio = $this->currencyConverter->convert($from_currency, $to_currency, $amount = 1);
    }

    if (!$conversion_ratio) {
      throw new NotFoundHttpException();
    }

    $cookie_name = $this->currencyConverter->getConversionKeyValue($from_currency, $to_currency);

    $expire = $this->config->get('conversion_rate_lifetime');

    setcookie($cookie_name, $conversion_ratio, time() + $expire, "/");

    $data = [
      'ratio' => $conversion_ratio,
      'name' => $cookie_name,
      'to_currency' => $from_currency,
    ];

    return new JsonResponse($data);
  }

  /**
   * Set currency in cache.
   */
  public function setCurrencyToCache($from_currency, $to_currency) {
    $conversion_ratio = 1;
    $cache_conversion_ratio_list = [];
    $conversion_ratio_key = $this->currencyConverter->getConversionKeyValue($from_currency, $to_currency);
    $conversion_ratio_list_key = 'conversion_ratio_list';

    $cache = $this->cache->get($conversion_ratio_list_key);
    if ($cache && property_exists($cache, 'data')) {
      $cache_conversion_ratio_list = $cache->data;
    }

    if (array_key_exists($conversion_ratio_key, $cache_conversion_ratio_list)) {
      $conversion_ratio = $cache_conversion_ratio_list[$conversion_ratio_key];
      return new JsonResponse([
        'ratio' => $conversion_ratio,
        'name' => $conversion_ratio_key,
        'to_currency' => $from_currency,
      ]);
    }

    if ($from_currency != $to_currency) {
      $conversion_ratio = $this->currencyConverter->convert($from_currency, $to_currency, $amount = 1);
    }

    if (!$conversion_ratio) {
      throw new NotFoundHttpException();
    }

    $cache_conversion_ratio_list[$conversion_ratio_key] = $conversion_ratio;
    $this->currencyConverter->setCache($cache_conversion_ratio_list);

    return new JsonResponse([
      'ratio' => $conversion_ratio,
      'name' => $conversion_ratio_key,
      'to_currency' => $from_currency,
    ]);
  }

}
