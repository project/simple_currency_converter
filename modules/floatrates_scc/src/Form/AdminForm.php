<?php

namespace Drupal\floatrates_scc\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Admin form.
 */
class AdminForm extends ConfigFormBase {

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\Config
   */
  public $config;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Constructs a new form.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    parent::__construct($config_factory);

    $this->config = $config_factory->getEditable('floatrates_scc.settings');
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'floatrates_scc_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['floatrates_scc.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $key = 'feed_url';

    $default_value = $this->config->get($key);

    $description = $this->t('Please specify the JSON Feed URL to be used.');

    $form[$key] = [
      '#type' => 'textfield',
      '#title' => $this->t('Feed URL'),
      '#default_value' => $default_value,
      '#description' => $description,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $key = 'feed_url';

    $url = $form_state->getValue($key);
    if (!UrlHelper::isValid($url, TRUE)) {
      $form_state->setErrorByName($key, $this->t('Invalid URL'));
    }

    try {
      $this->httpClient->request('GET', $url);
    }
    catch (\exception $exception) {
      $form_state->setErrorByName($key, 'Could not access the specified URL.');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach (Element::children($form) as $variable) {
      $this->config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }

    $this->config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

}
