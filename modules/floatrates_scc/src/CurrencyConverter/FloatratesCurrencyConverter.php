<?php

namespace Drupal\floatrates_scc\CurrencyConverter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\simple_currency_converter\CurrencyConverter;
use Drupal\simple_currency_converter\CurrencyConverter\CurrencyConverterInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Floatrates Class.
 */
class FloatratesCurrencyConverter implements CurrencyConverterInterface {

  /**
   * Provides HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Provides the config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Provides the module config.
   *
   * @var \Drupal\Core\Config
   */
  protected $config;

  /**
   * The Currency Converter.
   *
   * @var \Drupal\simple_currency_converter\CurrencyConverter
   */
  protected $currencyConverter;

  /**
   * Constructs a new object.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, CurrencyConverter $currencyConverter) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->config = $config_factory->get('floatrates_scc.settings');
    $this->currencyConverter = $currencyConverter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('cache.default'),
      $container->get('simple_currency_converter.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function convert($from_currency, $to_currency, $amount) {
    $output = NULL;
    try {
      $url = $this->config->get('feed_url');
      $request = $this->httpClient->get($url);
      $jsonBody = $request->getBody();
      $data = $this->parse($jsonBody);

      if (isset($data[$from_currency]) && isset($data[$to_currency])) {
        $from = $data[$from_currency]['rate'];
        $to = $data[$to_currency]['rate'];

        $output = ($to / $from) * $amount;
      }
    }
    catch (RequestException $e) {

    }
    return $output;
  }

  /**
   * Parse the json.
   */
  private function parse($raw_json) {
    try {
      $json = json_decode($raw_json);
    }
    catch (\Exception $e) {
      return FALSE;
    }

    $data = [];
    foreach ($json as $value) {
      $code = (string) $value->code;
      $rate = (string) $value->rate;

      $data[$code] = [
        'code' => $code,
        'rate' => $rate,
      ];

    }

    $data[$this->currencyConverter->getDefaultCurrency()] = [
      'code' => $this->currencyConverter->getDefaultCurrency(),
      'rate' => '1',
    ];

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function currencies() {
    return [
      'USD',
      'JPY',
      'BGN',
      'CZK',
      'DKK',
      'GBP',
      'HUF',
      'LTL',
      'PLN',
      'RON',
      'SEK',
      'CHF',
      'NOK',
      'HRK',
      'RUB',
      'TRY',
      'AUD',
      'BRL',
      'CAD',
      'CNY',
      'HKD',
      'IDR',
      'ILS',
      'INR',
      'KRW',
      'MXN',
      'MYR',
      'NZD',
      'PHP',
      'SGD',
      'THB',
      'ZAR',
    ];
  }

}
