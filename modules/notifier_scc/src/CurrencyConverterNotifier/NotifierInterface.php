<?php

namespace Drupal\notifier_scc\CurrencyConverterNotifier;

/**
 * The Notifier.
 */
interface NotifierInterface {

  /**
   * Processes a notification.
   */
  public function notify($data);

}
